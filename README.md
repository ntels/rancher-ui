Hubpop Rancher UI
--
* v2.1.22_hubpop: 사용자포탈 UI 소스
* v2.1.22_hubpop_admin: 관리자포탈 개발환경 소스
* v2.1.22_hubpop_admin_prod: 관리자포탈 운영환경 소스

프록시 서버
--
* 메뉴정보가져오는 프록시서버(nginx)
  * 31번서버 /WORK/nginx_ui_proxy
  * 포트: 34443
* 대시보드 프록시 서버(nginx)

Docker build
--
* git clone
```
git clone -b v2.1.22_hubpop https://gitlab.com/ntels/rancher-ui.git
```
* 작업폴더: 41번서버 /WORK/rancher_ui_prod/rancher-ui
* Docker Build
```
docker build --tag registry.hubpop.io:5000/rancher/ui:v2.1.22_prod_0807 .
```

version
--
* v2.1.22_prod_0807
* v2.1.22_prod_0829
  * 쿼터추가, 목록으로가기, 샘프코드 다운로드  



설치 및 실행
--------
* 설치 환경
 * 41번서버 
 * /WORK/rancher_ui/v2.1.21 폴더 참조
* UI 소스 clone
```
## 소스를 받으면 ui 폴더가 생긴다.
git clone -b v2.1.21 https://github.com/rancher/ui.git
```

* 한글파일 적용 및 빌드
 * 번역파일 최종파일: 41번 서버 /WORK/rancher_ui/ko-kr-file-v2.1.20-20190322.zip 
 * 한글파일 : ko-kr.yaml (마우스 오른클릭으로 다운로드)
```
## 한글파일은 clone 받은 폴더의 translations폴더에 copy한다.
## 빌드 (UI 인스턴스가 실행중인 경우 파일을 변경하면 자동으로 빌드되어 적용된다.)
cd 'ui'./scripts/update-dependencies
```

* 번역 파일은 아래와 같이 각기 다른 위치에 존재 
```
# 경고메뉴 관련 메시지 파일 
./lib/alert/translations/en-us.yaml 
# 로깅메뉴 관련 메시지 파일 
./lib/logging/translations/en-us.yaml 
# 파이프라인메뉴 관련 메시지 파일 
./lib/pipeline/translations/en-us.yaml 
# 위 3개를 제외한 나머지 메시지 파일 
./translations/en-us.yaml
```

* 실행: https://cs.hubpop.io:48000 으로 접속
 * default는 localhost:8000 이므로 ui포트를 변경하기 위해서 아래 파일에서 8000을 찾아서 48000으로 변경적용함
  * .config/environment.js
  * .ember-cli
  * lib/shared/addon/components/settings/danger-zone/component.js
 * UI 인스턴스 실행 방법
  * 첫번째 방법과 Docker 이미지로 build해서 실행하는 방법 추천
```
## 41번서버에 실행되고 있는 Rancher Server의 URL로 실행한다.(추천)
RANCHER="https://ks.hubpop.io/" yarn start


## 백그라운드에서 실행은 아래 커멘드 이용. 하지만 실행한 putty창을 닫으면 프로세스도 죽는다.

RANCHER="https://ks.hubpop.io/" nohup yarn start &

## 프로세스는 아래 커멘드로 찾아서 kill 한다.

netstat -tnlp | grep 48000

## docker 빌드

docker build --tag rancher/dev_ui:v2.1.21 .

## docker 실행(추천)

docker run -d -p 48000:48000 -e RANCHER="https://ks.hubpop.io/" --name dev-rancher-ui rancher/dev_ui:v2.1.21_r3
```

UI distribute
---

* https://github.com/rancher/ui#compiling-for-distribution

* rancher version v2.1.9 
* UI versibon v2.1.22

* Release 된 파일은 https://releases.rancher.com/ui/2.1.22.tar.gz 로 다운로드 할수 있다. 
* 압축 풀때는 tar xvzf 2.1.22.tar.gz --strip-components=1 를 이용한다.


변경된 UI를 빌드 후 Rancher Sever Docker Container 생성 방법
---

작업폴더 41번서버 /WORK/rancher_ui/build-ui-and-rancher-server

UI 소스 다운로드 git clone -b v2.1.22 https://github.com/rancher/ui.git

한글파일 수정

UI 빌드 UI를 빌드하면 ui/dist/static 폴더에 {version}.tar.gz 파일 생성.

```
cd ui
./scripts/build-static -f -s
(욥션: -f force , -s skip test 등등이 존재)

```
Rancher Server 소스 다운로드 https://github.com/rancher/rancher/releases/tag/v2.1.9 git clone -b v2.1.9 https://github.com/rancher/rancher.git

dapper 설치

curl -sL https://releases.rancher.com/dapper/latest/dapper-`uname -s`-`uname -m` > /usr/local/bin/dapper
chmod +x /usr/local/bin/dapper
빌드된 ui 파일을 Rancher의 Dockerfile이있는 폴더에 copy

cd /WORK/rancher_ui/build-ui-and-rancher-server/rancher-2.1.9/package
cp /WORK/rancher_ui/build-ui-and-rancher-server/ui-2.1.22/ui/dist/static/2.1.22.tar.gz ./
Dockerfile 수정

RUN mkdir -p /usr/share/rancher/ui && \ cd /usr/share/rancher/ui && \ curl -sL https://releases.rancher.com/ui/${CATTLE_UI_VERSION}.tar.gz | tar xvzf - --strip-components=1 && \

아래와 같이 변경한다. RUN mkdir -p /usr/share/rancher/ui && \ cp 2.1.22.tar.gz /usr/share/rancher/ui && \ cd /usr/share/rancher/ui && \ tar xvzf 2.1.22.tar.gz --strip-components=1 && \
Rancher Server 빌드

방법1. 아래 명령어로 단순히 package폴더에 있는 Dockerfile 만으로 빌드가 되지 않는다.

docker build --tag 172.30.1.31:5000/rancher:v2.1.9-r1 .
방법2. root폴더에 Dockerfile.dapper 을 실행하기 위해 Dapper 설치후 root폴더에서 dapper 실행했지만 중간에 멈춤현상 발생

dapper


##### 방법3. 이미만들어진 rancher 이미지에 빌드된 ui소스를 copy하여 새로운 이미지 생성

```
FROM rancher/rancher:v2.1.9

RUN cd /usr/share/rancher/ui && \ rm -rf *

COPY 2.1.22.tar.gz /usr/share/rancher/ui/

RUN
```


git에 Rancher UI push하기
---

##### 방법 1. github에서 zip 파일 이용

다운로드 (https://github.com/rancher/ui/releases/tag/v2.1.22)
압축을 풀고 gitlab에 push
개발서버에서 Clone 한 후 컴파일 및 실행 41번서버

##### 방법 2. github -> gitlab

gitlab으로 전부 이관후 이클립스에 버전 v2.1.22소스를 checkout
41번 서버에서 clone(git clone -b v2.1.22 https://gitlab.com/---/rancher-ui.git) 한후 빌드및 실행 테스트 --> 성공
이클립스에서 v2.1.22 로 변경후 한글파일 수정하여 commit/push 한다. 41번에서 clone 한후 빌드 및 실행 테스트
이클립스에서 v2.1.22 로 변경후 branch 생성 (v2.1.22-h1) 하고 한글파일을 수정하여 commit/push 한다 41번에서 clone 한후 빌드 및 실행 테스트 --> 성공
이때 icon 폴더를 commit/push 하지 않는다. vender/icon 폴더는 이클립스상에서 보이지 않으므로 주의 필요



Rancher UI
--------

Perhaps you like managing Cattle.

## Usage

Prerequisites:
* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) 8.x+ (with NPM)
* [Yarn](https://yarnpkg.com/en/docs/install) (Note Path Setup)

If you're on a Mac and use Homebrew, you can follow these steps:
```bash
  brew install node watchman yarn
```

Setup:
```bash
  git clone 'https://github.com/rancher/ui'
  cd 'ui'
  ./scripts/update-dependencies
```

Run development server:
```bash
  yarn start
```

Connect to UI at https://localhost:8000/ .  The server automatically picks up file changes, restarts itself, and reloads the web browser.  This is intended only for development, see below for distributing customizations.

Run development server pointed at another instance of the Rancher API
```bash
  RANCHER="https://rancher-server/" yarn start
```

RANCHER can also be `hostname[:port]` or `ip[:port]`.

### Compiling for distribution

Rancher releases include a static copy of the UI passed in during build as a tarball.  To generate that, run:
```bash
  ./scripts/build-static
```

### Customizing

We highly suggest making customizations as an [ember-cli addon](http://ember-cli.com/extending/#developing-addons-and-blueprints) rather than forking this repo, making a bunch of changes and then fighting conflicts to keep it up to date with upstream forever.  [ui-example-addon-machine](https://github.com/rancher/ui-example-addon-machine) is an example addon that adds a custom screen for a docker-machine driver.  If there is no way for you to get to what you want to change from an addon, PRs to this repo that add generalized hooks so that you can are accepted.

### Translations
Rancher UI supports localization via translations files. You can swap translations live by utilizing the Language Picker located in the footer. If you would like to add your own translations files follow the directions below.

- Fork the Rancher UI repo
- Copy the ```en-us.yaml``` file located in ```/translations``` folder and rename using the ```<language-code>/<country-code>.yaml``` format ([Supported Locales](https://github.com/andyearnshaw/Intl.js/tree/master/locale-data/jsonp))
- Replace the values on each key with you're new values corresponding to your language
- Ensure you replace the ```languageName``` value as this is what will be displayed in the language picker in the UI
- While developing you can use ```SHFT + L``` when not focused in an input or text area to toggle the languages between your currently selected language and a special *none* language to see what key values are missing
- When you've finished you're translations issue a pull request back to the Rancher UI repo to have your translation included

### Hosting remotely

If you want to customize the UI, re-packaging all of Rancher to distribute the UI is possible but not terribly convenient. Instead you can change Cattle to load the UI source from a remote web server:

- Build with `./scripts/build-static -l -c 'your-server.com'`
- Upload `./dist/static/latest2` so that it's available at https://your-server.com/latest2
  - It must be available over HTTPS.
  - You can rename the "latest2" part with the `-v` flag
- Change the value of http[s]://your-rancher:8080/v3/settings/ui-index to the same `https://yourserver.com/latest2` URL

### Running Tests

```bash
  yarn global add ember-cli
```

* `yarn lint:hbs`
* `yarn lint:js`
* `yarn lint:js -- --fix`

* `ember test`
* `ember test --server`

### Bugs & Issues
Please submit bugs and issues to [rancher/rancher](//github.com/rancher/rancher/issues) with a title starting with `[UI] `.

Or just [click here](//github.com/rancher/rancher/issues/new?title=%5BUI%5D%20) to create a new issue.


#### Useful links

* ember: http://emberjs.com/
* ember-cli: http://www.ember-cli.com/
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

License
=======
Copyright (c) 2014-2019 [Rancher Labs, Inc.](http://rancher.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
